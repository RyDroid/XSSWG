<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Affero General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Affero General Public License for more details.
      
      You should have received a copy of the GNU Affero General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:template match="subtitles">
    <p>Subtitles:</p>
    <xsl:apply-templates select="." mode="list" />
  </xsl:template>
  
  <xsl:template match="subtitles" mode="list">
    <ul>
      <xsl:apply-templates select="subtitle" mode="list-item" />
    </ul>
  </xsl:template>
  
  <xsl:template match="subtitle" mode="list-item">
    <li>
      <xsl:apply-templates select="." />
    </li>
  </xsl:template>
  
  <xsl:template match="subtitle">
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="." />
      </xsl:attribute>
      
      <xsl:choose>
	<xsl:when test="@language">
	  <xsl:apply-templates select="@language" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>unknown language</xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="@language">
    <xsl:choose>
      <xsl:when test=". = 'ar'">
	<xsl:text>العربية</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'az'">
	<xsl:text>azərbaycanca</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'ce'">
	<xsl:text>hохчийн</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'ca'">
	<xsl:text>català</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'ceb'">
	<xsl:text>sinugboanong binisaya</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'de'">
	<xsl:text>deutsch</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'es'">
	<xsl:text>español</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'eo'">
	<xsl:text>esperanto</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'en'">
	<xsl:text>english</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'fi'">
	<xsl:text>suomi</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'fr'">
	<xsl:text>français</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'fr-fr'">
	<xsl:text>français (France)</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'gl'">
	<xsl:text>galego</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'it'">
	<xsl:text>italiano</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'it-it'">
	<xsl:text>italiano (Italy)</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'jp'">
	<xsl:text>日本語</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'nl'">
	<xsl:text>nederlands</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'pl'">
	<xsl:text>polski</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'pt'">
	<xsl:text>português</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'pt-br'">
	<xsl:text>português (Brasil)</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'ru'">
	<xsl:text>pусский</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'sv'">
	<xsl:text>svenska</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'th'">
	<xsl:text>ภาษาไทย</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'tr'">
	<xsl:text>türkçe</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'vi'">
	<xsl:text>tiếng việt</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'vo'">
	<xsl:text>volapük</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'war'">
	<xsl:text>winaray</xsl:text>
      </xsl:when>
      <xsl:when test=". = 'zh'">
	<xsl:text>中文</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
