<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Affero General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Affero General Public License for more details.
      
      You should have received a copy of the GNU Affero General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:template match="tags" mode="meta-keywords">
    <xsl:element name="meta">
      <xsl:attribute name="name">keywords</xsl:attribute>
      <xsl:attribute name="content">
	<xsl:apply-templates select="." mode="meta-keywords-content" />
      </xsl:attribute>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="tags" mode="meta-keywords-content">
    <xsl:for-each select="tag">
      <xsl:value-of select="." />
      <xsl:if test="position() > 0 and position() != last()">
	<xsl:text>, </xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
