<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Affero General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Affero General Public License for more details.
      
      You should have received a copy of the GNU Affero General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:template match="content" mode="links">
    <xsl:choose>
      <xsl:when test="links">
	<xsl:apply-templates select="links" />
      </xsl:when>
      <xsl:when test="link">
	<p>
	  Download it:
	  <xsl:apply-templates select="link" mode="link" />
	</p>
      </xsl:when>
      <xsl:otherwise>
	<p>Download it: no link :(</p>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="links">
    <p>Download it:</p>
    <xsl:apply-templates select="." mode="list" />
  </xsl:template>
  
  <xsl:template match="links" mode="list">
    <ul>
      <xsl:apply-templates select="link" mode="list-item" />
    </ul>
  </xsl:template>
  
  <xsl:template match="link" mode="list-item">
    <li>
      <xsl:apply-templates select="." mode="link" />
    </li>
  </xsl:template>
  
  <xsl:template match="link" mode="link">
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="." />
      </xsl:attribute>
      <xsl:value-of select="." />
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
