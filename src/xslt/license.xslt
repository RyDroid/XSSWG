<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Affero General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Affero General Public License for more details.
      
      You should have received a copy of the GNU Affero General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:template match="license">
    <p>
      License:
      <xsl:choose>
	<xsl:when test="link">
	  <xsl:element name="a">
	    <xsl:attribute name="href">
              <xsl:value-of select="link" />
	    </xsl:attribute>
	    <xsl:apply-templates select="." mode="name-with-version" />
	  </xsl:element>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="." mode="name-with-version" />
	</xsl:otherwise>
      </xsl:choose>
    </p>
  </xsl:template>
  
  <xsl:template match="license" mode="name-with-version">
    <xsl:choose>
      <xsl:when test="(name or @name) and (version or @version)">
	<xsl:apply-templates select="." mode="name-only" />
	<xsl:text> </xsl:text>
	<xsl:apply-templates select="." mode="version" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates select="." mode="name-only" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="license" mode="name-only">
    <xsl:choose>
      <xsl:when test="name">
	<xsl:value-of select="name" />
      </xsl:when>
      <xsl:when test="@name">
	<xsl:value-of select="@name" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="license" mode="version">
    <xsl:choose>
      <xsl:when test="version">
	<xsl:value-of select="version" />
      </xsl:when>
      <xsl:when test="@version">
	<xsl:value-of select="@version" />
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
