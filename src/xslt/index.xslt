<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Affero General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Affero General Public License for more details.
      
      You should have received a copy of the GNU Affero General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:import href="tags.xslt" />
  <xsl:import href="title.xslt" />
  <xsl:import href="authors.xslt" />
  <xsl:import href="license.xslt" />
  <xsl:import href="links.xslt" />
  <xsl:import href="subtitles.xslt" />
  
  
  <xsl:output encoding             = "utf-8"
              indent               = "yes"
              method               = "xml"
              omit-xml-declaration = "yes"
              version              = "1.0" />
  
  
  <xsl:template match="website">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head>
	<meta charset="utf-8" />
	<xsl:apply-templates select="." mode="html-head-title" />
	<xsl:apply-templates select="." mode="open-graph-title" />
	<xsl:apply-templates select="tags" mode="meta-keywords" />
      </head>
      
      <body>
	<h1><xsl:apply-templates select="." mode="title-long" /></h1>
	
	<xsl:apply-templates select="." mode="contents" />
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="website" mode="contents">
    <xsl:choose>
      <xsl:when test="contents">
	<xsl:apply-templates select="contents" />
      </xsl:when>
      <xsl:otherwise>
	<p>No content. :(</p>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="content">
    <h2><xsl:value-of select="name" /></h2>
    
    <xsl:apply-templates select="description" />
    <xsl:apply-templates select="." mode="authors" />
    <xsl:apply-templates select="license" />
    <xsl:apply-templates select="." mode="links" />
    <xsl:apply-templates select="subtitles" />
  </xsl:template>
  
  <xsl:template match="description">
    <p>
      Description:
      <xsl:value-of select="." />
    </p>
  </xsl:template>
</xsl:stylesheet>
