<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!--
      Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
      
      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU Affero General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.
      
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
      GNU Affero General Public License for more details.
      
      You should have received a copy of the GNU Affero General Public License
      along with this program. If not, see <https://www.gnu.org/licenses/>.
  -->
  
  
  <xsl:template match="website" mode="open-graph-title">
    <xsl:if test="title">
      <xsl:element name="meta">
	<xsl:attribute name="property">og:title</xsl:attribute>
	<xsl:attribute name="content">
	  <xsl:apply-templates select="title" mode="long" />
	</xsl:attribute>
      </xsl:element>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="website" mode="html-head-title">
    <title><xsl:apply-templates select="." mode="title-short" /></title>
  </xsl:template>
  
  <xsl:template match="website" mode="title-short">
    <xsl:choose>
      <xsl:when test="title">
	<xsl:apply-templates select="title" mode="short" />
      </xsl:when>
      <xsl:otherwise>Share</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="website" mode="title-long">
    <xsl:choose>
      <xsl:when test="title">
	<xsl:apply-templates select="title" mode="long" />
      </xsl:when>
      <xsl:otherwise>Share</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="title" mode="short">
    <xsl:choose>
      <xsl:when test="short">
	<xsl:value-of select="short" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="title" mode="long">
    <xsl:choose>
      <xsl:when test="long">
	<xsl:value-of select="long" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="." />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
