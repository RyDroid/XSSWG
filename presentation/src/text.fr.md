# eXtensible Static Sharing Website Generator

## Sujet

Les services audio-visuelles privateurs et/ou centralisés peuvent poser de nombreux problèmes : [centralisation](https://www.fdn.fr/actions/confs/internet-libre-ou-minitel-2-0/), [logiciel privateur](https://www.gnu.org/philosophy/free-software-even-more-important.fr.html), [tris non neutres et secrets](http://www.numerama.com/politique/191933-facebook-piege-par-lillusion-de-neutralite-quil-veut-donner.html), [publicités lucratives](http://www.nicola-spanti.info/fr/documents/articles/computing/not-mine/publicite_macarons-du-diable.html), [censure](http://www.numerama.com/pop-culture/128130-quand-sony-exploite-une-video-4k-gratuite-et-accuse-lauteur-de-contrefacon.html), [vie privée](http://www.numerama.com/magazine/32617-l-ia-de-facebook-sait-ce-que-vous-faites-dans-vos-videos.html), etc.
J'ai commencé [un article (encore incomplet) qui explique plus longuement ces problèmes](http://www.nicola-spanti.info/fr/documents/articles/computing/mine/put-an-end-to-centralized-audio-video-services.html).

Heureusement, il n'y a pas de problème pour partager massivement une vidéo sans énorme infrastucture : il y a [le protocole BitTorrent](https://fr.wikipedia.org/wiki/BitTorrent_%28protocole%29).
Pour faire un site web réparti sur plusieurs machines à travers le monde (par exemple avec [Freenet](https://fr.wikipedia.org/wiki/Freenet) ou [ZeroNet](https://fr.wikipedia.org/wiki/ZeroNet)), pour limiter fortement la censure et les attaques par deni de services, il faut que les pages ne néccessitent pas d'être générées à chaque fois.
Il faut donc un site web statique (pas de langage serveur et pas de base de données sur le serveur, juste du HTML + CSS + potentiellement du JavaScript).
Il reste la question de l'abonnement, il y a heureusement le standard flux [Atom](https://fr.wikipedia.org/wiki/Atom)/[RSS](https://fr.wikipedia.org/wiki/RSS).

Puisque rien n'est générable sur le serveur, il faut générer le site web avant de l'envoyer.
Pour cela, on peut enregister les données en XML, puis un ou plusieurs filtres (en XSLT) peuvent générer des fichiers "utilisables" (page web, flux Atom/RSS, etc).
C'est sur ce principe qu'est fait [le blog de Bortzmeyer](http://www.bortzmeyer.org/blog-implementation.html).
Ces technologies sont simples et standardisées.

J'ai commencé à faire un logiciel pour ce cahier des charges : [XSSWG (eXtensible Static Sharing Website Generator)](https://gitlab.com/RyDroid/XSSWG).
Il génère des pages web, mais pas de flux Atom/RSS.
Le but premier est de l'améliorer pour la partie web (groupe de créateurs et créatrices, traduction, choix d'un design, documentation, exécutable, paquet pour une ou des distributions, tests unitaires, etc) et d'ajouter un filtre pour générer de l'Atom ou du RSS.
Vous pourriez aussi faire une interface graphique pour éditer le XML et faire les générations.

Le projet est sous [licence libre](https://www.gnu.org/licenses/license-list.fr.html) [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.fr.html)+.
Vos contributions devront être sous la même licence ou une compatible.
Le projet devra continuer de fonctionner sans [logiciel privateur](https://www.gnu.org/proprietary/proprietary.fr.html).
Si vous ne savez pas ce qu'est [le logiciel libre](https://www.gnu.org/philosophy/free-sw.fr.html), Richard Stallman l'a expliqué à [PSES 2016](https://data.passageenseine.org/2016/webm/stallman.webm) (en français) et au [TEDx en 2014](https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society) (en anglais mais plus court).

Vous devrez versionner le projet avec git.
Le projet devra être disponible via Internet (par exemple via [GitLab.com](https://gitlab.com/), ou [NotABug.org](https://notabug.org/), mais [GitHub n'est pas une option](http://carlchenet.com/2016/01/22/le-danger-github/)).

## Contact

- Client : Nicola Spanti, nicola.spanti@ecole.ensicaen.fr
