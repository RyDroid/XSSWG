# Copyright (C) 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Folders and files
SRC_DIR=src
XSLT_DIR=$(SRC_DIR)/xslt
XML_PATHS=$(XSLT_DIR):samples
BUILD_DIR=build

# Commands
XMLCHECK=xmllint --noout --path $(XML_PATHS)
XSLTPROC=xsltproc --path $(XML_PATHS)

# Package and archive
PACKAGE=XSSWG
FILES_TO_ARCHIVE=$(SRC_DIR)/ samples/ makefile \
	LICENSE_* *.md \
	.gitignore .editorconfig


all: tests presentations archive-default


tests: samples
	$(XMLCHECK) \
		tags.xslt \
		title.xslt \
		authors.xslt \
		license.xslt \
		links.xslt \
		subtitles.xslt \
		index.xslt \
		xslt-processors.xml \
		videos-stallman.xml \
		operating-systems.xml \
		empty.xml


samples: \
	$(BUILD_DIR)/samples/xslt-processors.html \
	$(BUILD_DIR)/samples/videos-stallman.html \
	$(BUILD_DIR)/samples/operating-systems.html \
	$(BUILD_DIR)/samples/empty.html

$(BUILD_DIR)/samples/xslt-processors.html: \
		$(XSLT_DIR)/*.xslt samples/xslt-processors.xml
	$(XSLTPROC) \
		--output $(BUILD_DIR)/samples/xslt-processors.html \
		index.xslt xslt-processors.xml

$(BUILD_DIR)/samples/videos-stallman.html: \
		$(XSLT_DIR)/*.xslt samples/videos-stallman.xml
	$(XSLTPROC) \
		--output $(BUILD_DIR)/samples/videos-stallman.html \
		index.xslt videos-stallman.xml

$(BUILD_DIR)/samples/operating-systems.html: \
		$(XSLT_DIR)/*.xslt samples/operating-systems.xml
	$(XSLTPROC) \
		--output $(BUILD_DIR)/samples/operating-systems.html \
		index.xslt operating-systems.xml

$(BUILD_DIR)/samples/empty.html: \
		$(XSLT_DIR)/*.xslt samples/empty.xml
	$(XSLTPROC) \
		--output $(BUILD_DIR)/samples/empty.html \
		index.xslt empty.xml


presentations:
	cd presentation && make


archives-all: archives-tar zip 7z

archives-tar: tar-gz tar-bz2 tar-xz

dist: archive-default

archive-default: tar-xz

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE)


clean: clean-build clean-bin clean-python clean-profiling clean-ide

clean-build:
	@$(RM) -rf -- build/ Build/ BUILD/

clean-bin:
	$(RM) -f -- *.o *.a *.so *.ko *.lo *.dll *.out

clean-python:
	$(RM) -rf -- *.pyc __pycache__

clean-profiling:
	$(RM) -f -- callgrind.out.*

clean-ide:
	$(RM) -f -- qmake_makefile *.pro.user *.cbp *.CBP

clean-tmp:
	$(RM) -rf -- \
		*~ *.bak *.backup .\#* \#* *.swp *.swap \
		*.sav *.save *.autosav *.autosave \
		*.log *.log.* error_log* \
		.cache/ .thumbnails/

clean-doc:
	@$(RM) -rf -- doc/ Doc/ documentation/ Documentation/

clean-archives:
	$(RM) -f -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav *.out \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.gz *.synctex.bz2 *.synctex.bzip *.synctex.xz *.synctex.lzma \
		*.synctex.zip *.synctex.7z *.synctex.rar

clean-git:
	git clean -fdx
