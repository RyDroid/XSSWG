# eXtensible Static Sharing Website Generator

XSSWG is a program to generate a static website to share contents.
It is [free/libre software](https://www.gnu.org/philosophy/free-sw.html).
It is based on [XSLT](https://en.wikipedia.org/wiki/XSLT).
Happy hacking and sharing!
